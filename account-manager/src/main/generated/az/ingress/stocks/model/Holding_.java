package az.ingress.stocks.model;

import az.ingress.stocks.common.model.Account;
import az.ingress.stocks.common.model.Holding;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Holding.class)
public abstract class Holding_ {

	public static volatile SingularAttribute<Holding, Account> accountId;
	public static volatile SingularAttribute<Holding, Long> numberOfShares;
	public static volatile SingularAttribute<Holding, String> stockId;
	public static volatile SingularAttribute<Holding, Long> id;
	public static volatile SingularAttribute<Holding, Double> averagePrice;

	public static final String ACCOUNT_ID = "accountId";
	public static final String NUMBER_OF_SHARES = "numberOfShares";
	public static final String STOCK_ID = "stockId";
	public static final String ID = "id";
	public static final String AVERAGE_PRICE = "averagePrice";

}

