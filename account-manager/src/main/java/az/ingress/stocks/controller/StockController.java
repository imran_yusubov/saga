package az.ingress.stocks.controller;

import az.ingress.stocks.common.dto.CreateOrderDto;
import az.ingress.stocks.common.dto.GenericSearchDto;
import az.ingress.stocks.common.dto.StockDto;
import az.ingress.stocks.common.model.PStock;
import az.ingress.stocks.dto.OrderDto;
import az.ingress.stocks.service.OrderService;
import az.ingress.stocks.service.StocksService;
import java.security.Principal;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("stocks")
public class StockController {

    private final OrderService orderService;
    private final StocksService stocksService;

    @PostMapping("/search")
    public Page<StockDto> searchAll(@RequestBody GenericSearchDto filter, Pageable pageable) {
        return stocksService.findAll(filter, pageable);
    }

    @PostMapping("/preferential")
    public Iterable<PStock> searchAllPreferential() {
        return stocksService.findAllPreferential();
    }

    @PostMapping("/orders")
    public OrderDto placeOrder(@Valid @RequestBody CreateOrderDto createOrderDto, Principal user) {
        OrderDto orderDto = orderService.placeOrder(createOrderDto, user.getName());
        return orderDto;
    }

    @PostMapping("/orders/search")
    public Page<OrderDto> searchAll(@RequestBody GenericSearchDto filter, Pageable pageable, Principal user) {
        return orderService.findAllOrders(filter, pageable, user.getName());
    }

}
