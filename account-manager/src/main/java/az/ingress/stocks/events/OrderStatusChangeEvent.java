package az.ingress.stocks.events;

import az.ingress.stocks.dto.OrderDto;
import org.springframework.context.ApplicationEvent;

public class OrderStatusChangeEvent extends ApplicationEvent {

    private final String oldStatus;
    private final OrderDto orderDto;

    public OrderStatusChangeEvent(Object source, String oldStatus, OrderDto orderDto) {
        super(source);
        this.oldStatus = oldStatus;
        this.orderDto = orderDto;
    }

    public String getOldStatus() {
        return oldStatus;
    }

    public OrderDto getOrderDto() {
        return orderDto;
    }
}
