package az.ingress.stocks.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class PreferentialStock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double open;
    private Double dayMax;
    private Double dayMin;
    private Double current;
    private String stockId;
    private boolean preferential;
}
