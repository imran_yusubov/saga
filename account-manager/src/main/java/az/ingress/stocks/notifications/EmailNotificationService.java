package az.ingress.stocks.notifications;

import az.ingress.stocks.common.dto.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmailNotificationService {

    private final String companyEmail;
    private final String emailsTopic;
    private final KafkaTemplate<String, Email> kafkaTemplate;

    public EmailNotificationService(@Value("${companyEmail}") String companyEmail,
            @Value("${kafka.topics.emails}") String emailsTopic,
            KafkaTemplate<String, Email> kafkaTemplate) {
        this.companyEmail = companyEmail;
        this.emailsTopic = emailsTopic;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendEmail(Email email) {
        email.setFrom(companyEmail);
        kafkaTemplate.send(emailsTopic, email);
    }

}
