package az.ingress.stocks.repository;

import az.ingress.stocks.common.model.user.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    Page<User> findAll(Pageable page);

    Optional<User> findByUserName(String userName);

}
