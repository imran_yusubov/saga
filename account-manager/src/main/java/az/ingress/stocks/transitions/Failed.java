package az.ingress.stocks.transitions;

import az.ingress.stocks.dto.OrderDto;
import az.ingress.stocks.model.orders.OrderStatus;
import az.ingress.stocks.model.orders.OrderTransition;
import org.springframework.stereotype.Service;

@Service
public class Failed implements OrderTransition {

    public static final String NAME = "failed";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.FAILED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
    }
}
