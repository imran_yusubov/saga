package az.ingress.stocks.transitions;

import az.ingress.stocks.common.dto.CreateOrderDto;
import az.ingress.stocks.dto.OrderDto;
import az.ingress.stocks.model.orders.OrderStatus;
import az.ingress.stocks.model.orders.OrderTransition;
import az.ingress.stocks.service.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Pending implements OrderTransition {

    public static final String NAME = "pending";

    private final ModelMapper modelMapper;
    private final KafkaTemplate<String, CreateOrderDto> createOrderTemplate;
    private final String createOrdersTopic;
    private final AccountService accountService;

    public Pending(
            ModelMapper modelMapper, KafkaTemplate<String, CreateOrderDto> createOrderTemplate,
            @Value("${kafka.topics.orders.create}") String createOrdersTopic,
            AccountService accountService) {
        this.modelMapper = modelMapper;
        this.createOrderTemplate = createOrderTemplate;
        this.createOrdersTopic = createOrdersTopic;
        this.accountService = accountService;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.PENDING;
    }

    @Override
    public void applyProcessing(OrderDto orderDto) {
        final CreateOrderDto createOrderDto = modelMapper.map(orderDto, CreateOrderDto.class);
        final String userName = accountService.findAccountById(orderDto.getAccount().getId()).getUser().getUserName();
        createOrderDto.setUser(userName);
        createOrderTemplate.send(createOrdersTopic, createOrderDto);
    }
}
