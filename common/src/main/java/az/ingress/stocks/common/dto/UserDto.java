package az.ingress.stocks.common.dto;

import az.ingress.stocks.common.model.user.UserRole;
import java.util.Date;
import lombok.Data;

@Data
public class UserDto {

    private long id;

    private String userName;
    private String password;
    private String email;
    private UserRole role;

    private Date created;
}
