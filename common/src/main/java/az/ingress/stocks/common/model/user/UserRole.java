package az.ingress.stocks.common.model.user;

public enum UserRole {
    ADMIN,
    CUSTOMER
}
