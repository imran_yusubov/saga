package az.ingress.stocks.common.repository;

import java.util.Optional;
import java.util.function.Function;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


/**
 * A base class to get a UserSearchView's field defined of type T by retriever,
 * check it with validate method and, if valid, convert to a predicate
 * @param <T> type of the field to apply criteria to (e.g. String or Date)
 * @param <SV> type of the search query view to extract the value of criteria field from using retriever
 *            (e.g. UserSearchView)
 * @param <E> the type of JPA entity to apply filtering to (e.g. UserProfile)
 */
public abstract class AbstractPredicateBuilder<T, SV, E> implements PredicateBuilder<T, SV, E> {

    private final Function<SV,T> retriever;

    /**
     * Creates an ew instances
     * @param retriever the function to extract the value of type T to be matched with a value of the criteria field
     *                  from the search query view of type SV
     */
    public AbstractPredicateBuilder(Function<SV, T> retriever) {
        this.retriever = retriever;
    }

    public Optional<Predicate> apply(Root<E> r, CriteriaBuilder b, SV searchView) {
        T value = retriever.apply(searchView);
        if (isBad(value)) {
            return Optional.empty();
        }
        return Optional.ofNullable(provide(r, b, value));
    }

    /**
     * The method returns true if the value is not good to be used for filtering (e.g. empty)
     * @param value value to test
     * @return false if value is good
     */
    protected abstract boolean isBad(T value);

    protected abstract Predicate provide(Root<E> root, CriteriaBuilder builder, T value);

    @FunctionalInterface
    protected interface PredicateProvider<T, E> {
        Predicate provide(Root<E> root, CriteriaBuilder bldr, T value);
    }
}
