package az.ingress.stocks.common.repository.redis;

import az.ingress.stocks.common.model.PStock;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PStocksRedisRepository extends CrudRepository<PStock, Long> {

    Optional<PStock> findByStockId(String stockId);

    List<PStock> findByPreferentialIsTrue();
}
