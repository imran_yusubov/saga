package com.nyse;

import com.nyse.repository.OrderRepository;
import az.ingress.stocks.common.config.LoggingTcpConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@Slf4j
@EnableAsync
@EnableScheduling
@SpringBootApplication
@Import({LoggingTcpConfiguration.class})
public class FakeExchangeApplication implements CommandLineRunner {

    @Autowired
    private OrderRepository orderRepository;

    public static void main(String[] args) {
        SpringApplication.run(FakeExchangeApplication.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Override

    public void run(String... args) throws Exception {
        log.info("Market opened");
    }

//    @Scheduled(fixedRate = 1000)
//    public void listOrders() {
//        System.out.println("Listing apple orders");
//        orderRepository.findByStockIdAndStatusAndMaxPriceGreaterThanAndType("AAPL", OrderStatus.NEW, 200.0, "buy")
//                .stream()
//                .forEach(System.out::println);
//    }
}
