package com.nyse.config;

import com.nyse.dto.StockDto;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    @Value("${kafka.bootstrapAddress}")
    private String bootstrapAddress;

    @Value("${kafka.consumer.groupId.orders}")
    private String kafkaOrdersConsumerGroupId;

    @Value("${kafka.consumer.groupId.orders_statuses}")
    private String kafkaOrderStatusesConsumerGroupId;

    public ConsumerFactory<String, StockDto> stockConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        doCommonConfig(props);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaOrdersConsumerGroupId);
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(),
                new JsonDeserializer<>(StockDto.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, StockDto> stocksKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, StockDto> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(stockConsumerFactory());
        return factory;
    }

    private void doCommonConfig(Map<String, Object> configProps) {
        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
    }


    @Bean
    Map<String, Object> kafkaConsumerConfiguration() {
        Map<String, Object> configuration = new HashMap<>();
        configuration.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        configuration.put(ConsumerConfig.GROUP_ID_CONFIG, "groupId");
        configuration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
       /// configuration.put(JsonDeserializer.TRUSTED_PACKAGES, "com.nyse");

        return configuration;
    }

    @Bean
    ReceiverOptions<String, StockDto> kafkaReceiverOptions( String[] inTopicName) {
        ReceiverOptions<String, StockDto> options = ReceiverOptions.create(kafkaConsumerConfiguration());
        return options.subscription(Arrays.asList("stocks"))
                .withKeyDeserializer(new StringDeserializer())
                .withValueDeserializer(new JsonDeserializer<>(StockDto.class));
    }

    @Bean
    @Scope("prototype")
    KafkaReceiver<String, StockDto> reactiveKafkaReceiver(ReceiverOptions<String, StockDto> kafkaReceiverOptions) {
        return KafkaReceiver.create(kafkaReceiverOptions);
    }

}
