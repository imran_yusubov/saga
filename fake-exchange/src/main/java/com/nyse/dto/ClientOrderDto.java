package com.nyse.dto;

import lombok.Data;

@Data
public class ClientOrderDto {

    private Long id;
    private String stockId; //SYMBOL
    private Double maxPrice;
    private String hookURL;
    private Long numberOfShares;
    private String type; //buy, sell
}
