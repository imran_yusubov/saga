package com.nyse.dto;

import lombok.Data;

@Data
public class StockDto {

    private Long id;
    private Double open;
    private Double close;
    private Double current;
    private String stockId;
    private String description;
    private String index;
}
