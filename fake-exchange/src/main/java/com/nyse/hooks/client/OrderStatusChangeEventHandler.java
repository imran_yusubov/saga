package com.nyse.hooks.client;

import com.nyse.events.OrderStatusChangeEvent;
import com.nyse.service.TransitionService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderStatusChangeEventHandler implements ApplicationListener<OrderStatusChangeEvent> {

    private final TransitionService transitionService;
    private final HookManager hookManager;

    @Override
    public void onApplicationEvent(OrderStatusChangeEvent event) {
        System.out.println("Order status changed from "+event.getOldStatus()+" to "+event.getOrderDto().getStatus());
        hookManager.sendResponse(event.getOrderDto());
    }
}