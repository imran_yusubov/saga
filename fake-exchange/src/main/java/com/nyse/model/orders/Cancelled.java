package com.nyse.model.orders;

import com.nyse.dto.OrderDto;
import com.nyse.hooks.client.HookManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Cancelled implements Transition {

    public static final String NAME = "cancelled";

    private final HookManager hookManager;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.CANCELLED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        hookManager.sendResponse(order);
    }

}
