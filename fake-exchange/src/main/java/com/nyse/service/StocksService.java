package com.nyse.service;

import com.nyse.dto.StockDto;
import com.nyse.repository.StocksRepository;
import az.ingress.stocks.common.dto.GenericSearchDto;
import az.ingress.stocks.common.repository.search.SearchSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class StocksService {

    private final StocksRepository repository;
    private final StocksRepository stocksRepository;
    private final ModelMapper modelMapper;

    public Page<StockDto> findAll(GenericSearchDto filter, Pageable pageable) {
        return stocksRepository.findAll(new SearchSpecification<>(filter.getCriteria()), pageable)
                .map(p -> modelMapper.map(p, StockDto.class));
    }

}
