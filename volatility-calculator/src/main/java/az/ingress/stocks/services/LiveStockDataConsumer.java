package az.ingress.stocks.services;


import az.ingress.stocks.common.dto.StockDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LiveStockDataConsumer {

    private final PreferentialStocksService preferentialStocksService;

    @KafkaListener(containerFactory = "stocksKafkaListenerContainerFactory", topics = "${kafka.topics.stocks.live}", groupId = "${kafka.consumer.stocks.live.groupId}")
    public void listen(StockDto message) {
        log.trace("Received message:" + message);
        preferentialStocksService.processPreferentialStocks(message);
    }

}
